# run the start script

```bash
% npx shadow-cljs release lambdakids
% ./scripts/start.sh
```

# run in Docker
```bash
% docker build -t lambdakids .
% ./scripts/start-docker.sh
```


then open browser http://localhost:3000

#copy files to container without redeploying
```bash
% docker cp public/. <container-name>:/app/public 
% docker cp public/. lambdakids:/app/public 
```

#start bash shell in docker
```bash
% docker exec -it lambdakids /bin/sh 
```

(ns lambdakids.core
  (:require ["highlight.js" :as hl]))

(.. hl highlightAll)

(defn init []
  (prn "init"))

(defn ^:export show-search []
  (let [tf (js/document.getElementById "search-text-field")]
    (set! (.. tf -style -display) "block")))

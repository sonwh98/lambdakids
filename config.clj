(load-file "src/clj/lambdakids/core.clj")

{:environment :production
 :port 3000
 :chp-dir lambdakids.core/public-dir
 :public-dir lambdakids.core/public-dir
 :index lambdakids.core/template
 :mime-types {"chp" "text/html" 
              "blog" "text/html"
              nil "text/html"}
 :bidi-routes ["/" [
                    ["" (fn [req]
                          (lambdakids.core/page-handler {:params {:page-num "0"}}))]
                    [[[#"\d+" :page-num]] lambdakids.core/page-handler]
                    [[[#"\d+" :year] "/"
                      [#"\d+" :month] "/"
                      [#"\d+" :day] "/"
                      [#".*" :file-name]] lambdakids.core/blog-handler]
                    [#".*\.chp"  stigmergy.chp/hiccup-page-handler]
                    ["search" lambdakids.core/search-handler]
                    ["edit" lambdakids.core/edit-handler]
                    ["save" lambdakids.core/save-handler]
                    ]]}


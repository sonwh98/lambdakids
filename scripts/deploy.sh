docker exec -it lambdakids rm -rf /lambdakids
docker cp .. lambdakids:/lambdakids
docker stop lambdakids
docker start lambdakids
docker exec --workdir=/lambdakids -td lambdakids  sh -c "/lambdakids/start.sh"

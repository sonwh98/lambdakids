(defproject lambdakids "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.11.1"]
                 [stigmergy/chp "0.1.0-SNAPSHOT"]]
  :plugins [[lein-ancient "1.0.0-RC3"]
            [lein-cljfmt "0.9.2"]]
  :cljfmt {:file-pattern #"(.blog$|.chp$|.clj$)"
           :paths ["public"]}
  :profiles {:dev {:source-paths ["src/clj"]
                   :repl-options {:init-ns user
                                  :timeout 120000}}})

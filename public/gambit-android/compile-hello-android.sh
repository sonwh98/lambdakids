
% gsc -c hello.scm
% gsc -link hello.c

#dynamic linking
% $TOOLCHAIN/bin/aarch64-linux-android30-clang -o hello hello.c hello_.c -I$GAMBIT/include -L$GAMBIT/lib -lgambit -lm -ldl

#static linking
% $TOOLCHAIN/bin/aarch64-linux-android30-clang -o hello hello.c hello_.c $GAMBIT/lib/libgambit.a -I$GAMBIT/include -L$GAMBIT/lib  -lm -ldl 

% adb push ./hello /data/local/tmp
% adb shell /data/local/tmp/hello

(def a (list 1 2 3)) ;;this is a list of 3 numbers
(def b '(1 2 3)) ;;same list of 3 numbers using the quote '

;;There are two basic operations on lists: first and rest
(prn "first element of a is "(first a))
(prn "the rest of a is " (rest a))




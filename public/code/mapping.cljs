(def numbers [1 2 3 4 5])

;;map is a function that takes two arguments: first argument is a function, the second argument is a collection like a list, vector, set or map
(def incremented-numbers (map inc numbers))
(def doubled-numbers (map (fn [n]
                            (* 2 n))
                          numbers))

(println "numbers -> incremented-numbers=" numbers "->" incremented-numbers)
(println "numbers -> doubled-numbers=" numbers "->" doubled-numbers)
(println "type of numbers=" (type numbers))
(println "type of doubled-numbers=" (type doubled-numbers))

;;mapping preserves structure of the original data
;;notice incremented-numbers and doubled-numbers contains the same number of elements as numbers
;;numbers is a vector but map returns a lazy sequence. Use mapv if you want the result to be a vector
;;instead of a lazy sequence

(def doubled-numbers-as-vector (mapv (fn [n]
                                       (* 2 n))
                                     numbers))

(println "doubled-numbers-as-vector=" doubled-numbers-as-vector)
(println "type of doubled-numbers-as-vector=" (type doubled-numbers-as-vector))

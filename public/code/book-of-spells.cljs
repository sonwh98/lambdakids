
(defn turn-draw
  "turn right by specified angle, then draw a straight line with given length"
  [angle length]
  (right angle)
  (forward length))

(defn polygon
  "draw an n-sided regular polygon with sides of given length"
  [n length]
  (let [angle (/ 360 n)]
    (repeat n #(turn-draw angle length))))

(defn triangle [length]
  (polygon 3 length))

(defn square [length]
  (polygon 4 length))

(defn pentagon [length]
  (polygon 5 length))

(defn hexagon [length]
  (polygon 6 length))

(defn heptagon [length]
  (polygon 7 length))

(defn octagon [length]
  (polygon 8 length))

(defn nonagon [length]
  (polygon 9 length))

(defn hectogon [length]
  (polygon 100 length))

(defn circle [radius]
  (let [n 1000
        fudge-factor 0.01 ;;used instead of doing trig calculations
        distance (* fudge-factor radius)]
    (polygon 1000 distance)))

(defn black-magic
  "What kind of black magic is this?!"
  []
  (doseq [n (range 10)]
    (polygon n 100)))

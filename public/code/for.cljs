;;for can be used to implement map
(defn my-map [f a-collection]
  (for [element a-collection]
    (f element)))

(def numbers [1 2 3 4 5])
(def incremented-numbers (my-map inc numbers))
(def doubled-numbers (my-map (fn [n]
                               (* 2 n))
                             numbers))

(println "numbers=" numbers)
(println "incremented-numbers=" incremented-numbers)
(println "doubled-numbers=" doubled-numbers)
(println "type of numbers=" (type numbers))
(println "type of doubled-numbers=" (type doubled-numbers))

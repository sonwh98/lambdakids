(ns math)

(require '[clojure.string :as str])

(defn alpha-add-one-digit [alpha-digit]
  (cond
    (= :a alpha-digit) [:b]
    (= :b alpha-digit) [:c]
    (= :c alpha-digit) [:d]
    (= :d alpha-digit) [:e]
    (= :e alpha-digit) [:f]
    (= :f alpha-digit) [:g]
    (= :g alpha-digit) [:h]
    (= :h alpha-digit) [:i]
    (= :i alpha-digit) [:j]
    (= :j alpha-digit) [:b :a]))

(defn alpha-arabic-digits-converter [alpha]
  (cond
    (= :a alpha) 0
    (= :b alpha) 1
    (= :c alpha) 2
    (= :d alpha) 3
    (= :e alpha) 4
    (= :f alpha) 5
    (= :g alpha) 6
    (= :h alpha) 7
    (= :i alpha) 8
    (= :j alpha) 9))

(defn arabic-alpha-digits-converter [arabic]
  (cond
    (= 0 arabic) :a
    (= 1 arabic) :b
    (= 2 arabic) :c
    (= 3 arabic) :d
    (= 4 arabic) :e
    (= 5 arabic) :f
    (= 6 arabic) :g
    (= 7 arabic) :h
    (= 8 arabic) :i
    (= 9 arabic) :j))

(defn arabic-alpha-converter [arabic]
  (map arabic-alpha-digits-converter arabic))

(defn alpha-arabic-converter [alpha]
  (map alpha-arabic-digits-converter alpha))

(defn alpha-plus-one [alpha-number]
  (let [last-digit (last alpha-number)
        all-but-last (drop-last alpha-number)
        second-digit (last all-but-last)
        all-but-two-last (drop-last 2 alpha-number)
        plus-one (alpha-add-one-digit last-digit)
        plus-ten (alpha-add-one-digit second-digit)]
    (cond
      (= alpha-number [:j]) [:b :a]
      (= (last alpha-number) :j) (concat all-but-two-last plus-ten [:a])
      :else (concat all-but-last plus-one))))

(defn alpha-plus [a b])

(+ 2 3)
(+ 2
   (+ (+ 1 1) 1))

(alpha-plus-one
 (alpha-plus-one [:b]) ;; (+ 1 1)
 )
x => (alpha-plus-one (alpha-plus-one :a))
;; given any number x, create an expression of a series of alpha-plus-one
;; for example,
;; :a => 0 (the identity)
;; :b (alpha-plus-one :a)
;; :c (alpha-plus-one (alpha-plus-one :a)) => (alpha-plus-one :b)
;; :d (alpha-plus-one (alpha-plus-one (alpha-plus-one :a)) => (alpha-plus-one :c))
;; recursive call? Constant incrementing in loop?

;;loop recur
;;recursion
;;fibbinachi

(defn alpha-plus [a b]
  (case b
    :a a
    :b (alpha-plus-one a)
    :c (alpha-plus-one (alpha-plus-one a))
    :d (alpha-plus-one (alpha-plus-one (alpha-plus-one a)))
    :e (alpha-plus-one (alpha-plus-one (alpha-plus-one (alpha-plus-one a))))
    :f (alpha-plus-one (alpha-plus-one (alpha-plus-one (alpha-plus-one (alpha-plus-one a)))))
    :g (alpha-plus-one (alpha-plus-one (alpha-plus-one (alpha-plus-one (alpha-plus-one (alpha-plus-one a))))))
    :h (alpha-plus-one (alpha-plus-one (alpha-plus-one (alpha-plus-one (alpha-plus-one (alpha-plus-one (alpha-plus-one a)))))))
    :i (alpha-plus-one (alpha-plus-one (alpha-plus-one (alpha-plus-one (alpha-plus-one (alpha-plus-one (alpha-plus-one (alpha-plus-one a))))))))
    :j (alpha-plus-one (alpha-plus-one (alpha-plus-one (alpha-plus-one (alpha-plus-one (alpha-plus-one (alpha-plus-one (alpha-plus-one (alpha-plus-one a)))))))))))

(defn do-countup [n loop-counter]
  (if (= loop-counter n)
    (do (prn n)
        (prn "done"))
    (do (prn loop-counter)
        (do-countup n (+ loop-counter 1)))))

(defn loop-helper [n loop-counter function]
  (if (= loop-counter n)
    nil
    (do (function)
        (loop-helper n (+ loop-counter 1) function))))

(defn custom-for [n function]
  (loop-helper n 0 function))

(alpha-plus [:b :e] :j)

(alpha-arabic-converter [:d])
(alpha-arabic-converter [:b :e])
(alpha-arabic-converter [:j])
(alpha-arabic-converter [:c :d])

(defn fib [n]
  (case n
    0 0
    1 1
    (+ (fib (- n 1))
       (fib (- n 2)))))

(fib 35)
(fib 5)

;;end refactor

(def addition-table {0 {0 0
                        1 1
                        2 2
                        3 3
                        4 4
                        5 5
                        6 6
                        7 7
                        8 8
                        9 9}
                     1 {0 1
                        1 2
                        2 3
                        3 4
                        4 5
                        5 6
                        6 7
                        7 8
                        8 9
                        9 10}
                     2 {0 2
                        1 3
                        2 4
                        3 5
                        4 6
                        5 7
                        6 8
                        7 9
                        8 10
                        9 11}
                     3 {0 3
                        1 4
                        2 5
                        3 6
                        4 7
                        5 8
                        6 9
                        7 10
                        8 11
                        9 12}
                     4 {0 4
                        1 5
                        2 6
                        3 7
                        4 8
                        5 9
                        6 10
                        7 11
                        8 12
                        9 13}
                     5 {0 5
                        1 6
                        2 7
                        3 8
                        4 9
                        5 10
                        6 11
                        7 12
                        8 13
                        9 14}
                     6 {0 6
                        1 7
                        2 8
                        3 9
                        4 10
                        5 11
                        6 12
                        7 13
                        8 13
                        9 14}
                     7 {0 7
                        1 8
                        2 9
                        3 10
                        4 11
                        5 12
                        6 13
                        7 14
                        8 15
                        9 16}
                     8 {0 8
                        1 9
                        2 10
                        3 11
                        4 12
                        5 13
                        6 14
                        7 15
                        8 16
                        9 17}
                     9 {0 9
                        1 10
                        2 11
                        3 12
                        4 13
                        5 14
                        6 15
                        7 16
                        8 17
                        9 18}})

(defn do-countdown [n]
  (if (= n 0)
    (prn "done")
    (do (prn n)
        (do-countdown (- n 1)))))

;; it makes steps (example using 4):
;;  4 != 0
;; (do-countup 4 (+ 0 1))
;;  4 != 1
;; (do-countup 4 (+ 0 1))
;;  4 != 2
;; (do-countup 4 (+ 0 1))
;;  4 != 3
;; (do-countup 4 (+ 0 1))
;;  4 == 4
;; print "done"

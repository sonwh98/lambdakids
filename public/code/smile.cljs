(defn turn-draw [angle length]
  (right angle)
  (forward length))

(defn polygon
  "draw an n-sided regular polygon with sides of given length"
  [n length]
  (let [angle (/ 360 n)]
    (repeat n #(turn-draw angle length))))

(defn circle [radius]
  (let [n 1000
        fudge-factor 0.01 ;;used instead of doing trig calculations
        distance (* fudge-factor radius)]
    (polygon 1000 distance)))

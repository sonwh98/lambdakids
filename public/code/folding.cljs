(def a '( 1 2 3 4 5)) ;;this is a list
(def b (cons 1
             (cons 2
                   (cons 3
                         (cons 4
                               (cons 5 nil)))))) ;;this is also a list in traditional LISP

;;In traditional LISP, a list is chain of cons cells with the last pair in the chain ending with nil
;;This is not true in Clojure but let's pretend it is because it makes it easier to explain fold.

;;folding is simply replacing the cons function with a combinding function f and the nil with an arbitary value z.
;;let's replace cons with + and the nil with z

(def z 0) ;; let's give z a value of 0. 

;;notice the right most expression is evaluated first
;;and folds/collapse on the right, thus the name right fold
(def right-fold-b (+ 1
                     (+ 2
                        (+ 3
                           (+ 4
                              (+ 5 z))))))



;;notice the left most expression is evaluated first
;;and folds/collapse on the left, thus the name left fold
(def left-fold-b (+ (+ (+ (+ (+ z 1)
                             2)
                          3)
                       4)
                    5))

;;folding the list b with the + function sums all the numbers in b
;;since combinding function + is associative meaning x+y is the same as y+x
;;right fold and left fold produces the same result
(println "right-fold-b=" right-fold-b)
(println "left-fold-b=" left-fold-b)



(def v [1 2 3]) ;; a vector of 3 numbers
(prn "(first v) =" (first v)) ;;first element of vector v
(prn "(rest v) =" (rest v)) ;;rest of vector v
(prn "(nth v 0) =" (nth v 0)) ;;use nth function to get first element of v. first element starts at 0
(prn "(nth v 1) =" (nth v 1)) ;;second element of vector v.
(prn "(nth v 2) =" (nth v 2)) ;;third element of vector v.

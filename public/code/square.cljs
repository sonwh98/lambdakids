(defn square [x]
  (* x x))

(prn "square of 4 is" (square 4))
(prn "square of 5 is" (square 5))

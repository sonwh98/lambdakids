
% wget https://raw.githubusercontent.com/technomancy/leiningen/stable/bin/lein
% chmod a+x lein
% sudo mv lein /usr/local/bin
% mkdir -p ~/.emacs.d/lisp
% wget -c https://www.emacswiki.org/emacs/download/doremi.el -O ~/.emacs.d/lisp/doremi.el
% wget -c https://www.emacswiki.org/emacs/download/doremi-cmd.el -O ~/.emacs.d/lisp/doremi-cmd.el
% wget -c https://raw.githubusercontent.com/clojure-emacs/clj-refactor.el/master/clj-refactor.el -O ~/.emacs.d/lisp/clj-refactor.el
% wget -c https://bit.ly/2NlZY2G -O ~/.emacs.d/init.el
% wget -c https://bit.ly/2uNg1ic -O ~/.tmux.conf

#install a webserver to serve the html,js,wasm files
% npm install -g http-server

# change into your project dir
% cd your-project

# use wasm-pack to build
% wasm-pack build --release --target web

# start webserver
% http-server                                                                                       

;;(##start-repl-server "localhost:44555")

(c-define (inc x) (int) int "inc" "extern"
          (+ x 1))

(c-define (fib x) (int) int "fib" "extern"
          (if (or  (= x 0)
                   (= x 1))
              x
              (+ (fib (- x 1))
                 (fib (- x 2)))))

(c-define (say_hello n) (char-string) char-string "say_hello" "extern"
  (string-append "Hello " n "!"))

;;copied from $GAMBIT/test/server.scm
(define (catch-all-errors thunk)
  (with-exception-catcher
   (lambda (exc)
     (write-to-string exc))
   thunk))

(define (write-to-string obj)
  (with-output-to-string
    '()
    (lambda () (write obj))))

(define (read-from-string str)
  (with-input-from-string str read))

; The following "c-define" form will define the function "eval_string"
; which can be called from C just like an ordinary C function.  The
; single argument is a character string (C type "char*") and the
; result is also a character string.

(c-define (eval-string str) (char-string) char-string "eval_string" "extern"
  (catch-all-errors
   (lambda () (write-to-string (eval (read-from-string str))))))


(c-define (start-repl) () void "start_repl" "extern"
          (##start-repl-server "localhost:44555")
          (pp "read ")
          (read)
          (pp "done")
          )


% gsc -c slib.scm
% gsc -link slib.c #___LNK_slib__ symbol used in main.c generated here
% gcc slib.c slib_.c main.c -D___LIBRARY -I$GAMBIT/include $GAMBIT/lib/libgambit.a -lm -ldl -lutil  -lssl -lcrypto
% ./a.out                                                                                                                                                                             
Hello World from C

From Scheme:
say_hello("Sonny")=Hello Sonny!
inc(2)=3 
fib(15)=610
eval_string("(+ 1 1)")=2

#or alternatively build object files with gsc -obj
% gsc -c slib.scm
% gsc -link slib.c #___LNK_slib__ symbol used in main.c generated here
% gsc -obj -cc-options -D___LIBRARY slib.c slib_.c # -cc-options -D___LIBRARY necessary to supress generation of main function
% gsc -obj main.c
% gcc slib.o slib_.o main.o -I$GAMBIT/include $GAMBIT/lib/libgambit.a -lm -ldl -lutil  -lssl -lcrypto
% ./a.out                                                                                                                                                                             
Hello World from C

From Scheme:
say_hello("Sonny")=Hello Sonny!
inc(2)=3 
fib(15)=610
eval_string("(+ 1 1)")=2



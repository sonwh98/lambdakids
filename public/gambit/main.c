
#include <stdio.h>
#include <stdlib.h>

#include "slib.h"

int main(int argc, char** argv) {
  printf("Hello World from C\n\n");

  GAMBIT* g = setup_gambit();

  //printf("From Scheme:\n");

  /* char* result = eval_string (argv[1]); */
  /* printf("%s = %s\n", argv[1], result); */
  /* ___release_string (result); */

  start_repl();


  cleanup_gambit(g);
  
  return 0;
}


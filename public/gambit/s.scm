
(c-declare "extern int inc ();") ;;c functions must be declared

(define inc (c-lambda (int) int "inc")) ;;function implemented in C code

(pp (map inc '(1 2 3 4)))



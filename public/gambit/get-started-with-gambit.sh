
% sudo apt-get install build-essential
% git clone git@github.com:gambit/gambit.git
% cd gambit
% ./configure --prefix=/usr/local/gambit --enable-single-host --enable-targets=js --enable-default-compile-options="(compactness 9)" \
             --enable-multiple-versions --enable-openssl --enable-default-runtime-options=f8,-8,t8 --enable-poll
% make #compile
% make modules #compile modules
% make check
% make doc
% sudo make install #installation directory is configured with --prefix=/usr/local/gambit
% sudo ldconfig #this makes sure the newly installed library in /usr/local/lib are reloaded
% sudo make uninstall #use only if you need to uninstall binaries in /usr/local

#ignore the warning below about --eanble-single-host
#See https://github.com/gambit/gambit/issues/502#issuecomment-600610891
************************************************************************** 
***                                                                    *** 
*** The option "--enable-single-host" was not specified to the         *** 
*** configure script.  The Gambit system will compile correctly but    *** 
*** typically the executables will run considerably slower than when   *** 
*** "--enable-single-host" is specified.  On the other hand the build  *** 
*** with "--enable-single-host" is typically slower and requires lots  *** 
*** of RAM memory (>= 1 GB).  If you are willing to wait for the       *** 
*** longer build, try the configure option "--enable-single-host".     *** 
***                                                                    *** 
************************************************************************** 

% gsi -:d$ #starts gambit scheme interpreter with remote REPL running on port 44555
Gambit v4.9.3-1404-gf738de00

> (println "hello world")
hello world

